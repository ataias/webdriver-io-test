const AxeBuilder = require("@axe-core/webdriverio").default;
const { remote } = require("webdriverio");

(async () => {
  const client = await remote({
    logLevel: "error",
    capabilities: {
      browserName: "safari",
    },
  });

  await client.url("https://dequeuniversity.com/demo/mars/");

  const builder = new AxeBuilder({ client });
  try {
    const results = await builder.analyze();
    console.log(results);
  } catch (e) {
    console.error(e);
  }
})();
