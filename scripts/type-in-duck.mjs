import { remote } from "webdriverio";

const browser = await remote({
  capabilities: {
    browserName: "safari",
  },
});

await browser.url("https://duckduckgo.com");

const inputElem = await browser.$("#search_form_input_homepage");
await inputElem.setValue("WebdriverIO");

const submitBtn = await browser.$("#search_button_homepage");
await submitBtn.click();
